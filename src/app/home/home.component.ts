import {Component, inject} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HousingLocationComponent} from '../housing-location/housing-location.component';
import {HousingLocation} from '../housinglocation';
import {HousingService} from '../housing.service';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [CommonModule, HousingLocationComponent],
  template: `
    <section>
      <form>
        <input type="text" placeholder="Filter by city" #filter />
        <button class="primary" type="button" (click)="filterResults(filter.value)">Search</button>
        <button (click)="callResourceServer()" style="margin-left: 10vw; background-color: #00B8D4
;border: none;" type="button">Call Resource</button>
      </form>
      
    </section>
    <span style="margin-top: 1vh;">{{message}}</span>
    <section class="results">
      <app-housing-location
        *ngFor="let housingLocation of filteredLocationList"
        [housingLocation]="housingLocation"
      ></app-housing-location>
    </section>
  `,
  styleUrls: ['./home.component.css'],
})
export class HomeComponent {
  housingLocationList: HousingLocation[] = [];
  housingService: HousingService = inject(HousingService);
  filteredLocationList: HousingLocation[] = [];
  constructor(private httpClient: HttpClient,private snackBar: MatSnackBar) {
    this.housingLocationList = this.housingService.getAllHousingLocations();
    this.filteredLocationList = this.housingLocationList;
  }
  filterResults(text: string) {
    if (!text) {
      this.filteredLocationList = this.housingLocationList;
    }

    this.filteredLocationList = this.housingLocationList.filter((housingLocation) =>
      housingLocation?.city.toLowerCase().includes(text.toLowerCase()),
    );
  }

  
  message:string="";

  callResourceServer(){
    this.httpClient.get('/api/hello', { responseType: 'text' }).subscribe({
      next: (response) => (this.openSnackBar(response)),
      error: (error) => (this.message=error),
      complete: () => console.info('complete'),
    });
  }

  openSnackBar(message: string, action?: string) {
      this.snackBar.open(message, action, {
        duration: 3000,
      });
    }

}
